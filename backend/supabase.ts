import { createClient } from "@supabase/supabase-js";

const API_URL = "https://sbppbcdejidhoguvnkoa.supabase.co";
const ANON_KEY =
  "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyb2xlIjoiYW5vbiIsImlhdCI6MTYyMDQxMzEzNywiZXhwIjoxOTM1OTg5MTM3fQ.sIwWh4x_2HN1nI-F2l0UvMt7_lS3HkNSUn0owQjNzW0";

export const supabase = createClient(API_URL, ANON_KEY);
