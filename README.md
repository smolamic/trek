# Trek
An SQL-GUI in elm and php

## Goals
+ Very little dependencies
+ Generic over many use-cases for a database
+ Once a database is setup it should be usable for end-users
+ Easy setup on shared webspace

## Setup
### Build
To build the javascript bundle and generate the rest of the necessary files you need
+ nodejs
+ npm
+ php
+ composer

Run
```sh
make prod
```

### Run
After you've built the project you might want to deploy it.

The recommended way to run a trek-instance is with apache.

To do so you need:

+ an apache web server that runs php7 cgi scripts and supports UrlRewrite
+ a mysql/mariadb compatible database

Then you need to
+ adjust config.php with valid database information
+ point an apache vhost to dist/

### Build Docker
To build the docker image you need to first go through the build-step

You also need:
+ docker
+ docker-compose

Then just run
```sh
make docker/prod
```
The app will be served on localhost:8070

## Development
There are different ways to work on trek. The easiest of them is to run
the full dev-server in docker:

### Docker dev-server
You will need
+ docker
+ docker-compose

Then just run
```sh
make docker/dev
```

This approach is batteries included. The app will be served on localhost:8080

### Docker server
Alternatively you can run webpack-dev-server locally while running the backend
in a docker container.

To do so, you need
+ nodejs
+ npm
+ php
+ composer
+ docker
+ docker-compose

Then, to start the server, run
```sh
make docker/server
```

And to run the dev-server run
```sh
make dev
```

The app will be served on localhost:8080.

You may change both server and client code and will see updates immediately.

### Apache
To work without docker in your dev-environment, you may use apache to serve the
backend.

To do so you need
+ nodejs
+ npm
+ php
+ composer
+ an apache web server that runs php7 cgi scripts and supports UrlRewrite
+ a mysql/mariadb compatible database

To run trek this way, point an apache vhost to dist/, either set
`TREK_PROXY_HOST` to the url that points to your vhost (in my case this is
`http://localhost/trek/dist`) or adjust the `devServer.proxy['/api']` value
in webpack.dev.js.

Then run
```sh
make dev
```
That's it.

The app will be served on localhost:8080.

### Tests
There are two ways of running the test suite for the backend:

#### Docker
To run the tests with docker you need:
+ docker
+ docker-compose

Just run
```sh
make docker/test
```

#### Locally
To run the tests on your local machine you need:
+ php
+ phpunit
+ composer
+ a mysql/mariadb compatible database

Adjust server/test/bootstrap.php with valid database configuration

Then run
```sh
make test
```
or first
```sh
make server/enum
```
and then manually in server/test/
```sh
phpunit --bootstrap bootstrap.php
```

To generate documentation you need phpDocumentor

Then just run
```sh
make doc
```

To generate code coverage analysis you need xdebug

Then just run
```sh
make coverage
```
