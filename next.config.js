const path = require("path");

module.exports = {
  future: {
    webpack5: true,
  },
  webpack: (config) => {
    config.resolve.alias["react"] = path.resolve(
      __dirname,
      "node_modules",
      "react"
    );
    config.resolve.alias["react-dom"] = path.resolve(
      __dirname,
      "node_modules",
      "react-dom"
    );
    config.resolve.alias["apps"] = path.resolve(__dirname, "apps");
    config.resolve.alias["components"] = path.resolve(__dirname, "components");
    config.resolve.alias["backend"] = path.resolve(__dirname, "backend");
    return config;
  },
};
