import { parsePagination } from "./Pagination";

describe("Pagination", () => {
  it("parses a simple pagination string", () => {
    expect(parsePagination(100, "0:30")).toEqual({
      offset: 0,
      pageSize: 30,
    });
  });
});
