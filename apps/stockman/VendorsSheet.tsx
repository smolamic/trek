import * as React from "react";
import * as UI from "trek-ui";
import { Anchor } from "../PathContext";
import { parsePagination, Pagination } from "../Pagination";
import { supabase, definitions } from "backend";

const DEFAULT_PAGE_SIZE = 30;

export type VendorsSheetParams = {
  appId: string;
  vendorsActive?: string;
  vendorsPagination?: string;
};

export type Vendor = {
  key: string;
  name: string;
};

export type VendorsSheetProps = {
  data: Vendor[];
  count: number;
  activeRow?: string;
  offset: number;
  pageSize: number;
};

export async function fetchVendors({
  vendorsActive,
  vendorsPagination,
  appId,
}: VendorsSheetParams): Promise<VendorsSheetProps> {
  const { offset, pageSize } = parsePagination(
    DEFAULT_PAGE_SIZE,
    vendorsPagination
  );

  const { data, error, count } = await supabase
    .from<definitions["stockman_vendors"]>("stockman_vendors")
    .select("id, name", { count: "estimated" })
    .eq("app", appId);

  const rows = data.map((row) => ({
    name: row.name,
    key: row.id.toFixed(),
  }));

  return { data: rows, count, activeRow: vendorsActive, offset, pageSize };
}

export const VendorsSheet: React.FC<VendorsSheetProps> = ({
  data,
  count,
  activeRow,
  offset,
  pageSize,
}) => {
  const renderTRHead = React.useCallback(makeRenderTRHead(offset, pageSize), [
    offset,
    pageSize,
  ]);
  const renderPagination = React.useCallback(makeRenderPagination(pageSize), [
    pageSize,
  ]);

  return (
    <UI.Sheet
      columns={[
        <UI.Column type="string" name="name">
          Name
        </UI.Column>,
      ]}
      data={data}
      count={count}
      pageSize={30}
      activeRow={activeRow}
      renderTRHead={renderTRHead}
      renderPagination={renderPagination}
    />
  );
};

const makeRenderPagination = (pageSize: number) => {
  const renderButton = (props: UI.ButtonProps, page: number) => {
    const offset = (page - 1) * pageSize;
    const href = `vendors/${offset}:${pageSize}`;
    return (
      <Anchor href={href}>
        <UI.Button {...props} />
      </Anchor>
    );
  };
  return (props: UI.PaginationProps): JSX.Element => (
    <UI.Pagination {...props} renderButton={renderButton} />
  );
};

const makeRenderTRHead = (offset: number, pageSize: number) => (
  props: UI.TRHeadProps,
  data: Vendor
): JSX.Element => {
  const href = `vendors/${offset}:${pageSize}/${data.key}`;
  return (
    <Anchor href={href}>
      <UI.TRHead {...props} />
    </Anchor>
  );
};
