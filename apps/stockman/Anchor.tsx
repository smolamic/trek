import Link from "next/link";
import { useAppId } from "../AppIdContext";

export type AnchorProps = {
  href: string;
  children: React.ReactNode;
};

export const Anchor: React.FC<AnchorProps> = ({ href, children }) => {
  const appId = useAppId();

  return (
    <Link href={`/apps/stockman/${appId}/${href}`}>
      <a>{children}</a>
    </Link>
  );
};
