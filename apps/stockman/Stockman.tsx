import {
  VendorsSheet,
  VendorsSheetProps,
  VendorsSheetParams,
} from "./VendorsSheet";
import { PathProvider } from "../PathContext";

export type StockmanParams = { appId: string } & VendorsSheetParams;

export type StockmanProps = {
  vendorsSheetProps: VendorsSheetProps;
  appId: string;
};

export const Stockman: React.FC<StockmanProps> = ({
  appId,
  vendorsSheetProps,
}) => {
  const path = `stockman/${appId}`;
  return (
    <PathProvider path={path}>
      <VendorsSheet {...vendorsSheetProps} />
    </PathProvider>
  );
};
