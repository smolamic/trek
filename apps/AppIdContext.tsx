import * as React from "react";

const AppIdContext = React.createContext<string | null>(null);

export const AppIdProvider: React.FC<{
  appId: string;
  children: React.ReactNode;
}> = ({ appId, children }) => (
  <AppIdContext.Provider value={appId}>{children}</AppIdContext.Provider>
);

export function useAppId(): string {
  const appId = React.useContext(AppIdContext);

  if (appId === null) {
    throw new Error("Missing AppId");
  }

  return appId;
}
