export type Pagination = {
  offset: number;
  pageSize: number;
};

export function parsePagination(
  fallbackPageSize: number,
  def?: string
): Pagination {
  if (def === undefined) {
    return {
      offset: 0,
      pageSize: fallbackPageSize,
    };
  }

  const [offsetString, pageSizeString] = def.split(":");

  const offset = parseInt(offsetString, 10);
  const pageSize = Math.min(parseInt(pageSizeString, 10), 100);

  if (Number.isNaN(offset)) {
    throw new Error("Offset is not a number");
  }

  if (Number.isNaN(pageSize)) {
    throw new Error("PageSize is not a number");
  }

  if (offset < 0) {
    throw new Error("Forbidden negative offset");
  }

  if (pageSize < 0) {
    throw new Error("Forbidden negative pageSize");
  }

  return {
    offset,
    pageSize,
  };
}
