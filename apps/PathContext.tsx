import * as React from "react";
import Link from "next/link";

const PathContext = React.createContext<string>("/app");

export const PathProvider: React.FC<{
  path: string;
  children: React.ReactNode;
}> = ({ path, children }) => {
  const basePath = React.useContext(PathContext);
  const fullPath = `${basePath}/${path}`;

  return (
    <PathContext.Provider value={fullPath}>{children}</PathContext.Provider>
  );
};

export const Anchor: React.FC<{ href: string; children: React.ReactNode }> = ({
  href,
  children,
}) => {
  const basePath = React.useContext(PathContext);
  const fullHref = `${basePath}/${href}`;

  return (
    <Link href={fullHref}>
      <a>{children}</a>
    </Link>
  );
};
