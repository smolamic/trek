import { AppProps } from "next/app";
import { Reset } from "trek-ui";

const MyApp: React.FC<AppProps> = ({ Component, pageProps }) => {
  return (
    <>
      <Reset />
      <Component {...pageProps} />
    </>
  );
};

export default MyApp;
