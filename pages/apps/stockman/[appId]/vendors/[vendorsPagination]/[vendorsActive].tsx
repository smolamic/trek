import {
  Stockman,
  StockmanProps,
  StockmanParams,
} from "apps/stockman/Stockman";
import { fetchVendors } from "apps/stockman/VendorsSheet";
import { GetServerSideProps } from "next";
import { supabase, definitions } from "backend";

export default Stockman;

export const getServerSideProps: GetServerSideProps<
  StockmanProps,
  StockmanParams
> = async (ctx) => {
  const { appId } = ctx.params;

  const vendorsSheetProps = await fetchVendors(ctx.params);

  return {
    props: { appId, vendorsSheetProps },
  };
};
