import {
  StockmanProps,
  Stockman,
  StockmanParams,
} from "apps/stockman/Stockman";
import { GetServerSideProps } from "next";
import { supabase } from "backend/supabase";

export default Stockman;

export const getServerSideProps: GetServerSideProps<
  StockmanProps,
  StockmanParams
> = async (ctx) => {
  const { appId } = ctx.params;

  const { data, error, count } = await supabase
    .from("stockman_vendors")
    .select("id, name", { count: "estimated" })
    .eq("app", appId);

  data.forEach((row) => (row.key = row.id));

  return {
    props: { vendorsSheetProps: { data, count, appId } },
  };
};
